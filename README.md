# TinDog Website, my rendition of code #

This is the second project of many projects done over the span of doing this course.

This is basically me, recreating the "TinDog" website, with some of my minor tweaking and refactoring, and is mostly my code for all of it.

This introduced me to Bootstrap and responsive websites, and how to use them in CSS. 

This was accomplished in full before the end of Section 7 in the Course.